package com.example.hakan.mobildemo.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.hakan.mobildemo.R;
import com.example.hakan.mobildemo.model.MainDataModel;
import com.example.hakan.mobildemo.service.ServiceFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartFragment extends Fragment implements Callback<MainDataModel> {

    private onStartGameListener mOnStartGameListener;
    private Button mButtonStart;
    private ProgressBar mProgressBar;

    public StartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);

        mButtonStart = view.findViewById(R.id.buttonStart);
        mProgressBar = view.findViewById(R.id.progressBar);
        mButtonStart.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);

        mButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButtonStart.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                ServiceFactory.getInstance().getData(StartFragment.this);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onStartGameListener) {
            mOnStartGameListener = (onStartGameListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnStartGameListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnStartGameListener = null;
        ServiceFactory.getInstance().cancelListener();
    }

    @Override
    public void onResponse(Call<MainDataModel> call, Response<MainDataModel> response) {
        if (response.isSuccessful()) {
            MainDataModel mainDataModel = response.body();
            if (mainDataModel != null) {
                mOnStartGameListener.onStartGame(mainDataModel);
            } else {
                alertUser();
            }
        } else {
            alertUser();
        }
    }

    @Override
    public void onFailure(Call<MainDataModel> call, Throwable t) {
        t.printStackTrace();
        alertUser();
    }

    public void alertUser() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).setTitle(R.string.error).setMessage(R.string.cannot_fetch_data).create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mButtonStart.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    public interface onStartGameListener {
        void onStartGame(MainDataModel mainDataModel);
    }
}
