package com.example.hakan.mobildemo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hakan.mobildemo.R;
import com.example.hakan.mobildemo.model.MainDataModel;
import com.example.hakan.mobildemo.ui.GameSceneLayout;

public class GameFragment extends Fragment {

    private static final String ARG_MAIN_DATA = "main_data";

    private MainDataModel mMainDataModel;
    private GameSceneLayout mGameSceneLayout;
    private GameSceneLayout.GameCompletedListener mGameCompletedListener;

    public GameFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GameFragment.
     */
    public static GameFragment newInstance(MainDataModel mainDataModel) {
        GameFragment fragment = new GameFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_MAIN_DATA, mainDataModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMainDataModel = getArguments().getParcelable(ARG_MAIN_DATA);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game, container, false);
        mGameSceneLayout = view.findViewById(R.id.root);
        mGameSceneLayout.initLayout(mMainDataModel);
        mGameSceneLayout.setonGameCompletedListener(mGameCompletedListener);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GameSceneLayout.GameCompletedListener) {
            mGameCompletedListener = (GameSceneLayout.GameCompletedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement GameCompletedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mGameSceneLayout.setonGameCompletedListener(null);
    }
}
