package com.example.hakan.mobildemo.service;

import com.example.hakan.mobildemo.model.MainDataModel;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceFactory {

    private MockyService mMockyService;
    private MynetService mMynetService;
    private static ServiceFactory mServiceFactory;

    public static ServiceFactory getInstance() {
        if (mServiceFactory == null) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Retrofit retrofitMocky = new Retrofit.Builder()
                    .baseUrl("http://www.mocky.io/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Retrofit retrofitMynet = new Retrofit.Builder()
                    .client(client)
                    .baseUrl("https://appsoyun.mynet.com/")
                    .build();

            mServiceFactory = new ServiceFactory(retrofitMocky, retrofitMynet);
        }
        return mServiceFactory;
    }

    private ServiceFactory(Retrofit retrofitMocky, Retrofit retrofitMynet) {
        mMockyService = retrofitMocky.create(MockyService.class);
        mMynetService = retrofitMynet.create(MynetService.class);
    }


    public void getData(Callback<MainDataModel> callback) {
        mMockyService.mainDataModelCall().enqueue(callback);
    }

    public void cancelListener() {
        mMockyService.mainDataModelCall().cancel();
    }

    public void sendResult(int timeRemaining) {
        mMynetService.postResult(timeRemaining);
    }
}
