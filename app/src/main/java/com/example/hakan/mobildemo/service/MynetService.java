package com.example.hakan.mobildemo.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MynetService {

    @GET("demo/game.php?")
    Call<Void> postResult(@Query("time_remaning") int timeRemaining);
}
