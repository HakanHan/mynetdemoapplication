package com.example.hakan.mobildemo.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.constraint.Constraints;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;

import com.example.hakan.mobildemo.R;
import com.example.hakan.mobildemo.model.BallDataModel;
import com.example.hakan.mobildemo.model.MainDataModel;
import com.example.hakan.mobildemo.model.PositionModel;

public class GameSceneLayout extends ConstraintLayout implements View.OnTouchListener {

    public interface GameCompletedListener {
        void onGameCompleted();
    }

    private GameCompletedListener mGameCompletedListener;
    private MainDataModel mMainDataModel;
    private float mCircleDx, mCircleDy;
    private Paint mPaint;
    private Rect mCircleContainerRect;


    private View mRedView;
    private View mYellowView;
    private View mGreenView;
    private View mBlueView;

    private SparseArray<PositionModel> mCircleInitialPositions = new SparseArray<>();

    private static final long ANIMATION_DURATION = 300;

    public GameSceneLayout(Context context) {
        super(context);
    }

    public GameSceneLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameSceneLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void initLayout(final MainDataModel mainDataModel) {
        mMainDataModel = mainDataModel;
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(getResources().getColor(R.color.circle_container_color));

        post(new Runnable() {
            @Override
            public void run() {
                mRedView = findViewById(R.id.redView);
                mYellowView = findViewById(R.id.yellowView);
                mGreenView = findViewById(R.id.greenView);
                mBlueView = findViewById(R.id.blueView);

                initializeCircles();
                initializeBoxes();

            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mPaint != null && mCircleContainerRect != null) {
            canvas.drawRect(mCircleContainerRect, mPaint);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mCircleDx = view.getX() - event.getRawX();
                mCircleDy = view.getY() - event.getRawY();
                break;

            case MotionEvent.ACTION_MOVE:
                view.animate()
                        .x(event.getRawX() + mCircleDx)
                        .y(event.getRawY() + mCircleDy)
                        .setDuration(0)
                        .start();
                break;
            case MotionEvent.ACTION_UP:
                CircleView circleView = (CircleView) view;
                checkIfCircleOnCorrectBox(circleView);

            default:
                return false;
        }
        return true;
    }

    /**
     * Add circles dynamically according to service data and set correct size and positions for each
     *
     * @see CircleView
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initializeCircles() {
        int width = getWidth();
        int circleCount = mMainDataModel.getBalls().size();
        int horizontalMargin = getResources().getDimensionPixelSize(R.dimen.horizontal_margin);
        int verticalMargin = getResources().getDimensionPixelSize(R.dimen.vertical_margin);
        float totalMargin = (circleCount + 1) * horizontalMargin;
        int circleSize = (int) ((width - totalMargin) / circleCount);

        //Draw container background for circles
        mCircleContainerRect = new Rect();
        mCircleContainerRect.left = 0;
        mCircleContainerRect.right = getWidth();
        mCircleContainerRect.top = 0;
        mCircleContainerRect.bottom = circleSize + verticalMargin * 2;
        setWillNotDraw(false);
        invalidate();

        for (int i = 1; i <= circleCount; i++) {
            BallDataModel ballDataModel = mMainDataModel.getBalls().get(i - 1);
            LayoutParams params = new Constraints.LayoutParams(circleSize, circleSize);
            final CircleView circleView = new CircleView(getContext());
            circleView.setColor(ballDataModel.getColor());
            circleView.setLayoutParams(params);
            circleView.setId(i);
            circleView.setOnTouchListener(this);
            addView(circleView);

            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(this);
            if (i == 1) {
                constraintSet.connect(circleView.getId(), ConstraintSet.TOP, this.getId(), ConstraintSet.TOP, verticalMargin);
                constraintSet.connect(circleView.getId(), ConstraintSet.LEFT, this.getId(), ConstraintSet.LEFT, horizontalMargin);
            } else {
                constraintSet.connect(circleView.getId(), ConstraintSet.TOP, i - 1, ConstraintSet.TOP);
                constraintSet.connect(circleView.getId(), ConstraintSet.LEFT, i - 1, ConstraintSet.RIGHT, horizontalMargin);
            }
            constraintSet.applyTo(this);
            post(new Runnable() {
                @Override
                public void run() {
                    PositionModel positionModel = new PositionModel(circleView.getX(), circleView.getY());
                    mCircleInitialPositions.put(circleView.getId(), positionModel);
                }
            });
        }
    }


    /**
     * Set correct size and position for boxes
     */
    private void initializeBoxes() {
        View circleView = getChildAt(0);
        float horizontalMargin = getResources().getDimension(R.dimen.horizontal_margin);
        float verticalMargin = getResources().getDimension(R.dimen.vertical_margin);
        int totalHorizontalMargin = (int) (horizontalMargin * 3);
        int boxWidth = (this.getWidth() - totalHorizontalMargin) / 2;

        int totalVerticalMargin = (int) ((int) (verticalMargin * 3) + circleView.getHeight() + verticalMargin);
        int boxHeight = (this.getHeight() - totalVerticalMargin) / 2;

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);
        constraintSet.connect(mRedView.getId(), ConstraintSet.TOP, this.getId(), ConstraintSet.TOP, (int) (circleView.getHeight() + verticalMargin));
        constraintSet.applyTo(this);

        mRedView.getLayoutParams().height = boxHeight;
        mRedView.getLayoutParams().width = boxWidth;
        mGreenView.getLayoutParams().height = boxHeight;
        mGreenView.getLayoutParams().width = boxWidth;
        mYellowView.getLayoutParams().height = boxHeight;
        mYellowView.getLayoutParams().width = boxWidth;
        mBlueView.getLayoutParams().height = boxHeight;
        mBlueView.getLayoutParams().width = boxWidth;

    }

    private void checkIfCircleOnCorrectBox(CircleView circleView) {
        Rect circleRect = new Rect();
        circleView.getGlobalVisibleRect(circleRect);
        View view = null;
        switch (circleView.getColor()) {
            case Color.RED:
                view = mRedView;
                break;
            case Color.BLUE:
                view = mBlueView;
                break;
            case Color.GREEN:
                view = mGreenView;
                break;
            case Color.YELLOW:
                view = mYellowView;
                break;
        }

        if (view == null) return;

        Rect redViewRect = new Rect();
        view.getGlobalVisibleRect(redViewRect);
        if (redViewRect.contains(circleRect)) {
            moveCircleOnBox(circleView, view);
        } else {
            moveCircleOnInitialPosition(circleView);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void moveCircleOnBox(CircleView circleView, View boxView) {
        circleView.setOnTouchListener(null);
        Rect boxRect = new Rect();
        boxView.getGlobalVisibleRect(boxRect);
        int x;
        int y = boxRect.top + (boxRect.bottom - boxRect.top) / 2 - circleView.getHeight();

        if (boxView.getTag() == null) {
            x = boxRect.left + circleView.getWidth();
            boxView.setTag("");
        } else {
            x = boxRect.right - circleView.getWidth() * 2;
        }
        circleView.animate().x(x).y(y).setDuration(ANIMATION_DURATION).start();
        mCircleInitialPositions.delete(circleView.getId());
        if (mCircleInitialPositions.size() == 0) {
            //Game completed successfully
            mGameCompletedListener.onGameCompleted();
        }
    }

    private void moveCircleOnInitialPosition(CircleView circleView) {
        PositionModel positionModel = mCircleInitialPositions.get(circleView.getId());
        circleView.animate().x(positionModel.getX()).y(positionModel.getY()).setDuration(ANIMATION_DURATION).start();
    }

    public void setonGameCompletedListener(GameCompletedListener listener) {
        this.mGameCompletedListener = listener;
    }
}
