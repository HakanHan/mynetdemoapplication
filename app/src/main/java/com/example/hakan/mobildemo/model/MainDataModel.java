package com.example.hakan.mobildemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class MainDataModel implements Parcelable {
    private int seconds_remaining;
    private ArrayList<BallDataModel> balls;

    protected MainDataModel(Parcel in) {
        seconds_remaining = in.readInt();
    }

    public static final Creator<MainDataModel> CREATOR = new Creator<MainDataModel>() {
        @Override
        public MainDataModel createFromParcel(Parcel in) {
            return new MainDataModel(in);
        }

        @Override
        public MainDataModel[] newArray(int size) {
            return new MainDataModel[size];
        }
    };

    public int getSeconds_remaining() {
        return seconds_remaining;
    }

    public void setSeconds_remaining(int seconds_remaining) {
        this.seconds_remaining = seconds_remaining;
    }

    public ArrayList<BallDataModel> getBalls() {
        return balls;
    }

    public void setBalls(ArrayList<BallDataModel> balls) {
        this.balls = balls;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(seconds_remaining);
    }
}
