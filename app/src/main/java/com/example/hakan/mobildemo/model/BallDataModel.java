package com.example.hakan.mobildemo.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BallDataModel implements Parcelable {
    private int id;
    private String color;

    protected BallDataModel(Parcel in) {
        id = in.readInt();
        color = in.readString();
    }

    public static final Creator<BallDataModel> CREATOR = new Creator<BallDataModel>() {
        @Override
        public BallDataModel createFromParcel(Parcel in) {
            return new BallDataModel(in);
        }

        @Override
        public BallDataModel[] newArray(int size) {
            return new BallDataModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(color);
    }
}
