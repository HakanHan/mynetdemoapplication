package com.example.hakan.mobildemo.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.hakan.mobildemo.R;
import com.example.hakan.mobildemo.fragment.GameFragment;
import com.example.hakan.mobildemo.fragment.StartFragment;
import com.example.hakan.mobildemo.model.MainDataModel;
import com.example.hakan.mobildemo.service.ServiceFactory;
import com.example.hakan.mobildemo.ui.GameSceneLayout;

public class MainActivity extends AppCompatActivity implements StartFragment.onStartGameListener, GameSceneLayout.GameCompletedListener {

    private String mTimeLeftString;
    private TextView mTimeLeftTextView;
    private CountDownTimer mCountDownTimer;
    private int remainingTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTimeLeftTextView = findViewById(R.id.timeLeftTextView);
        mTimeLeftString = getString(R.string.time_left);
        replaceFragment(new StartFragment());
    }

    @Override
    public void onStartGame(MainDataModel mainDataModel) {
        int seconds = mainDataModel.getSeconds_remaining();
        String timeString = getTimeString(seconds);
        timeString = mTimeLeftString + timeString;
        mTimeLeftTextView.setText(timeString);
        mTimeLeftTextView.setVisibility(View.VISIBLE);
        replaceFragment(GameFragment.newInstance(mainDataModel));
        startTimer(seconds);
    }

    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
    }

    private String getTimeString(int secondsRemaining) {
        String time;
        int mins = secondsRemaining / 60;
        int seconds = secondsRemaining - mins * 60;
        String secondString;
        if (seconds < 10) {
            secondString = 0 + "" + seconds;
        } else {
            secondString = String.valueOf(seconds);
        }
        time = mins + " : " + secondString;
        time = mTimeLeftString + time;
        return time;
    }

    private void startTimer(int seconds) {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
        mCountDownTimer = new CountDownTimer(seconds * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                String timeString = getTimeString(seconds);
                mTimeLeftTextView.setText(timeString);
                remainingTime = seconds;
            }

            @Override
            public void onFinish() {
                replaceFragment(new StartFragment());
                mTimeLeftTextView.setVisibility(View.GONE);
                remainingTime = 0;
                showFailedDialog();
            }
        }.start();
    }

    private void showFailedDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setMessage(R.string.time_is_up).create();
        alertDialog.show();
    }

    @Override
    public void onGameCompleted() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        ServiceFactory.getInstance().sendResult(remainingTime);
        AlertDialog alertDialog = new AlertDialog.Builder(this).setMessage(R.string.game_completed_successfully).create();
        alertDialog.show();
        replaceFragment(new StartFragment());
        mTimeLeftTextView.setVisibility(View.GONE);
        remainingTime = 0;
    }
}
