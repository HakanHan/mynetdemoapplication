package com.example.hakan.mobildemo.service;

import com.example.hakan.mobildemo.model.MainDataModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MockyService {
    @GET("v2/5ac4c5ca2f00005600f5fbba")
    Call<MainDataModel> mainDataModelCall();
}
